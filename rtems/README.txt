/*
 * =====================================================================================
 *
 *       Filename:  README.txt
 *
 *    Description:  Port of midas to RTEMS. This is to firstly build rtems i386,pc386 bsp
 *		            with vme support for vme universe, vme tsi148, but this is not required. 
 *                  This work was done with the help of Till Straumann(SLAC) and 
 *                  Dr. Joel Sherrill(RTEMS), Chris Johns (RTEMS)
 *
 *        Version:  1.0
 *        Created:  07/06/2018 06:23:35
 *       Revision:  none
 *       Compiler:  
 *
 *         Author:  Lee Pool(LCP), lcpool@tlabs.ac.za 
 *   Organization:  iThemba LABS(iTL), Faure, South Africa.
 *
 * =====================================================================================
 */

# Below is initial instructions to get the port working.


# RTEMS libbsd uses waf for configure/build/install.
# This is required.
 wget https://waf.io/waf-2.0.8
 chmod +x waf-2.0.8 
 ln -s waf-2.0.8 waf
 

# Create working directory called sandbox
mkdir sandbox
cd sandbox/

# Get the required repos

#this repo to build rtems-tools set, for a specific architecture.
#this was tested with arch=i386
git clone git://git.rtems.org/rtems-source-builder.git

#provides bsd style network support 
git clone git://git.rtems.org/rtems-libbsd.git

#rtems source tree
git clone git://git.rtems.org/rtems.git

# build rtems tool set for i386 arch
cd rtems-source-builder/
$ git checkout 4.11
cd rtems/
../source-builder/sb-set-builder --list-bsets
../source-builder/sb-check 
../source-builder/sb-set-builder --prefix="/home/lcp/sandbox/rtems-4.11" 4.11/rtems-i386

#bootstrap rtems source
cd /home/lcp/sandbox/rtems
git checkout 4.11

#### this is to make changes to i386, pc386 board support package, to support vme universe,vme tsi148 chipsets.
#### this step is not required for midas rtems port to work. Only if you intend on using a frontend to read VME modules.
#### when adjusting Makefile.am --- must run, eg.: ~/sandbox/rtems/c/src/lib/libbsp/i386/pc386$ /home/lcp/sandbox/rtems/ampolish3 Makefile.am > preinstall.am

PATH="/home/lcp/sandbox/rtems-4.11/bin/:$PATH" ./bootstrap

#Build and install the RTEMS Board Support Packages (BSP) you want to use. In this example the path is /home/lcp/sandbox/rtems-4.11:
mkdir pc386
cd pc386
PATH="/home/lcp/sandbox/rtems-4.11/bin:$PATH" /home/lcp/sandbox/rtems/configure --target=i386-rtems4.11 --prefix="/home/lcp/sandbox/rtems-4.11" --disable-networking --enable-posix --enable-cxx --enable-multiprocessing ---enable-rdbg -enable-rtemsbsp=pc386
PATH="/home/lcp/sandbox/rtems-4.11/bin:$PATH" make
PATH="/home/lcp/sandbox/rtems-4.11/bin:$PATH" make install 


cd /home/lcp/sandbox/rtems-libbsd/
git checkout 4.11
git submodule init
git submodule update rtems_waf
$ waf configure --prefix="/home/lcp/sandbox/rtems-4.11"
$ waf
$ waf install 



export RTEMS_MAKEFILE_PATH=/home/lcp/sandbox/rtems-4.11/i386-rtems4.11/pc386/
$ cd /home/lcp/sandbox/midas/rtems

### Before running make, these changes in the Makefile is required for now.

SYSINC_DIR = /home/lcp/sandbox/rtems-4.11/i386-rtems4.11/pc386/lib/include
CC = /home/lcp/sandbox/rtems-4.11/bin/i386-rtems4.11-gcc
LD = /home/lcp/sandbox/rtems-4.11/i386-rtems4.11/bin/ld
AR = /home/lcp/sandbox/rtems-4.11/i386-rtems4.11/bin/ar
CC_DEFINES	 = -DOS_RTEMS $(CPU) -DDM_DUAL_THREAD

#### run make ####

$ make


#### the libmidas.o should be in 
$/home/lcp/sandbox/midas/rtems/rtemsobj
$ ls -l
hv.o
libmidas.a
libmidas.o
mfe.o
 midas.o
mrpc.o
multi.o
mxml.o
odb.o
strlcpy.o
system.o

#### An library archive can then be crated if required. ####
This completes the midas port.

##### a frontend application was tested #####
Busy moving frontend to new rtems libbsd api.


