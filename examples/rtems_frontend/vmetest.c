/*
 * =====================================================================================
 *
 *       Filename:  vmetest.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  06/07/2009 08:36:47
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */

#define CONFIGURE_INIT

/* #####   HEADER FILE INCLUDES   ################################################### */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

/* #####   HEADER FILE INCLUDES   RTEMS ############################################# */
#include <rtems.h>
#include <rtems/shell.h>
#include <bsp/tty_drv.h>
#include <bsp.h>
#include <uart.h>

#include "vmetest.h"
#include <rtems/rtems_bsdnet.h>
#include <rtems/error.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "../networkconfig.h"


#define NSERVER		1
#define BASE_PORT	24742

#define DATA_SINK_HOST	((128 << 24) | (233 << 16) | (14 << 8) | 60)


/* #####   VARIABLES  -  LOCAL TO THIS SOURCE FILE   ################################ */
int remote_debug = 0;
extern int BSPConsolePort;
extern rtems_task Init(rtems_task_argument argument);

#define PCI_MEM_BASE 0

rtems_task Init(rtems_task_argument ignored ) {
	int result = -1;
  	int status;
 	DWORD_t addr;
  	DWORD_t V792_addr = 0xee000000;
  	DWORD_t V1495_addr = 0x40000000;
  	DWORD_t V1190_addr = 0xd0000000;
 // 	DWORD_t V1495_addr = 0x40000000;

	DWORD_t vmebase_addr = V792_addr;

//	DWORD_t vmebase_addr = V1495_addr;

	DWORD_t pcibase_addr = 0xc0000000;
  
  	
	DWORD_t offset = 0x100e;
//	DWORD_t offset = 0x813d;
  	DWORD_t plocal = 0xfffff;

	int slot = 0;
	int channel = 0;

  	int AddrType[] 
                = {
                VME_AM_SUP_SHORT_IO,
                VME_AM_STD_SUP_DATA,
                VME_AM_EXT_USR_DATA,
                VME_AM_CSR,
		0x0d | VME_MODE_DBW16 
         };


	printf("\n **** BSP_vme_config .... **** \n" );
	BSP_vme_config();
 
	
	
	BSP_VMEOutboundPortCfg(channel, VME_AM_EXT_SUP_DATA | VME_MODE_DBW16 ,V1190_addr,	pcibase_addr,	0x10000);

//	BSP_VMEOutboundPortCfg(channel, VME_AM_EXT_SUP_DATA | VME_MODE_DBW16 ,vmebase_addr,	pcibase_addr,	0x10000);
	BSP_VMEOutboundPortCfg(++channel, VME_AM_EXT_SUP_DATA | VME_MODE_DBW16  ,V1495_addr,	0xb0000000,	0x10000);
	BSP_VMEOutboundPortCfg(++channel, VME_AM_EXT_SUP_DATA | VME_MODE_DBW16 ,V792_addr,	0xcf000000,	0x10000);
//	BSP_VMEOutboundPortCfg(++channel, VME_AM_EXT_SUP_DATA | VME_MODE_DBW16 ,V1495_addr,	0xcff00000,	0x10000);

		BSP_VMEOutboundPortsShow(NULL);


	result = BSP_vme2local_adrs( VME_AM_EXT_SUP_DATA | VME_MODE_DBW16  , vmebase_addr , &plocal );
	if( result >= 0 ) {
	printf("\n PORT: %d | ADDRESS: %lx | READ: %2x \n ", slot, (plocal + offset),*(int*)(plocal + offset));

	
/*	
	int i = 0x0;
	for( i = 0x0; i < 0x20; i++ ) {
		offset = 0x1000 + i;
        	printf("\n i = [%d] || PORT: %d | ADDRESS: %lx | READ: %2x \n ",i,slot, (plocal + offset),*(int*)(plocal + offset));
		usleep(1000000);
	}
*/	
	
	}else{
		printf(" Read Failed, result = %d  \n", result);
		printf("\n PORT: %d | ADDRESS: %lx | READ: %2x \n ", slot, (plocal + 0x813f),*(int*)(plocal + 0x813f));


	}

	   printk( "Initialize network\n" );
  rtems_bsdnet_initialize_network ();
  printk( "Network initialized\n" );
  rtems_bsdnet_show_inet_routes ();
 

rtems_status_code sc;
   printf ("Starting shell....\n\n");
   sc = rtems_shell_init ("fstst", 60 * 1024, 150, "/dev/console", 0, 1, NULL);
   if (sc != RTEMS_SUCCESSFUL)
     printf ("error: starting shell: %s (%d)\n", rtems_status_text (sc), sc);


	
	printf("\n **** STOP ***** \n");
    	exit( 0 );
    
}

