#!/bin/bash
#===============================================================================
#
#          FILE:  rscript.sh
# 
#         USAGE:  ./rscript.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  22/06/2009 15:32:25 SAST
#      REVISION:  ---
#===============================================================================

ar x ../libbsdport.a 
ar x ../libif_em.a
ar x ../libif_pcn.a
ar x ../libif_fxp.a
ar x ../libif_le.a
ar x ../libif_re.a
ar x ../libif_bge.a

cp -v * /home/lee/rbuild2/network-demos/netdemo
cp -v * /home/lee/rbuild2/network-demos/http
cp -v * /home/lee/rbuild2/network-demos/rpc_demo
cp -v * /home/lee/rbuild2/network-demos/ttcp
